#include "ResultInfo.hh"
#include <iostream>

#include "Domain/ElemMapping/EntityLists.hh"
#include "General/Environment.hh"

namespace CoupledField {

  ResultInfo::ResultInfo() {

    resultType = NO_SOLUTION_TYPE;
    dofNames.Clear();
    unit = "";
    complexFormat = AMPLITUDE_PHASE;
    entryType = UNKNOWN;
    definedOn = FREE;
    fromOptimization = false;
    isStatic = false;
  }


  UInt ResultInfo::GetDofIndex( const std::string & dof ) const {

    Integer pos = dofNames.Find( dof );
    if( pos < 0  ) {
      EXCEPTION( "Dof with name '" << dof << "' not found!" );
    }
    return (UInt) (pos);
  }


  void ResultInfo::SetVectorDOFs(UInt dim, bool is_axi)
  {
    if(dim == 3)
      dofNames = "x", "y", "z";
    if(dim == 2 && !is_axi)
      dofNames = "x", "y";
    if(dim == 2 && is_axi)
      dofNames = "r", "z";
  }

  void ResultInfo::SetVectorDOFs(UInt dim, bool is_axi, bool is2p5)
  {
    if(dim == 3 || is2p5)
      dofNames = "x", "y", "z";
    else
    {
      if(dim == 2 && !is_axi)
        dofNames = "x", "y";
      if(dim == 2 && is_axi)
        dofNames = "r", "z";
    }
  }

  std::string ResultInfo::GetDofName( const UInt dof ) const {
    if( dof <= 0 || dof > dofNames.GetSize()+1 ) {
      EXCEPTION( "'dof' must be in the range of [1.." 
                 << dofNames.GetSize()+1 << "]!" );
    }
    
    return dofNames[dof];
  }

  std::string ResultInfo::ToString() const 
  {
    std::ostringstream os;
    os << "name = " << resultName << " dofs " << dofNames.GetSize();
    return os.str();
  }



  ResultInfo& ResultInfo::operator=( const ResultInfo& data ) {

    resultType = data.resultType;
    resultName = data.resultName;
    dofNames = data.dofNames;
    unit = data.unit;
    isStatic = data.isStatic;
    complexFormat = data.complexFormat;
    entryType = data.entryType;
    definedOn = data.definedOn;
    return *this;
  }


  void ResultInfo::Enum2String(EntityUnknownType in, 
                               std::string& out ) {
    
    switch( in ) {
      
    case NODE: 
      out = "node";
      break;
    case EDGE:
      out = "edge";
      break;
    case FACE:  
      out = "face";
      break;
    case ELEMENT:  
      out = "element";
      break;
    case SURF_ELEM:
      out = "surfElement";
      break;
    case REGION:
      out = "region";
      break;
    case REGION_AVERAGE:
      out = "regionAverage";
      break;
    case SURF_REGION:  
      out = "surfRegion";
      break;
    case NODELIST:
      out = "nodeList";
      break;
    case COIL:
      out = "coil";
      break;
    case FREE:
      out = "free";
      break;
    default:
      EXCEPTION( "Conversion of " << in
                 << " to EntityUnkwownType not possible" );
    }
    
  }
  
  void  ResultInfo::String2Enum( const std::string& in,
                                  EntityUnknownType& out ) {

    if( in == "node")
      out =  NODE;
      else if( in == "edge")
        out = EDGE;
      else if( in == "face")
        out = FACE;
      else if( in == "element")
        out = ELEMENT; 
      else if( in == "surfElement")
        out = SURF_ELEM;
      else if( in == "region")
        out = REGION;
      else if( in == "regionAverage")
        out = REGION_AVERAGE;
      else if( in == "surfRegion")
        out = SURF_REGION;
      else if( in == "nodeList")
        out = NODELIST;
      else if( in == "coil")
        out = COIL;
      else if( in == "free")
        out = FREE;
      else {
        EXCEPTION( "Can not convert '" << in << "' to EntityUnknownType");
      }
  }

  bool operator==( const ResultInfo& a, const ResultInfo& b ) {
    return a.resultType == b.resultType && a.dofNames == b.dofNames &&
           a.definedOn == b.definedOn && a.entryType == b.entryType;
  }

  bool operator!=( const ResultInfo& a, const ResultInfo& b ) {
    return !(a == b);
  }

  bool operator<( const ResultInfo& a, const ResultInfo& b ) {
    //return !(a==b);
    return ( a.resultType  < b.resultType );
  }
  // ************************************************************************
  // ENUM INITIALIZATION
  // ************************************************************************

  // Definition of unknown entity types
  static EnumTuple unknownTypeTuples[] = 
  {
   EnumTuple(ResultInfo::NODE,  "NODE"), 
   EnumTuple(ResultInfo::EDGE,  "EDGE"),
   EnumTuple(ResultInfo::FACE,  "FACE"),
   EnumTuple(ResultInfo::ELEMENT,  "ELEMENT"),
   EnumTuple(ResultInfo::SURF_ELEM,  "SURF_ELEM"),
   EnumTuple(ResultInfo::REGION,  "REGION"),
   EnumTuple(ResultInfo::REGION_AVERAGE,  "REGION_AVERAGE"),
   EnumTuple(ResultInfo::SURF_REGION,  "SURF_REGION"),
   EnumTuple(ResultInfo::NODELIST,  "NODELIST"),
   EnumTuple(ResultInfo::COIL,  "COIL"),
   EnumTuple(ResultInfo::FREE,  "FREE")
  };

  Enum<ResultInfo::EntityUnknownType> ResultInfo::EntityUnknownTypeEnum_ = \
      Enum<ResultInfo::EntityUnknownType>("Entry type of Results",
                                          sizeof(unknownTypeTuples) / sizeof(EnumTuple),
                                          unknownTypeTuples);

  // Definition of entry types of resultinfo
  static EnumTuple entryTypeTuples[] = 
  {
   EnumTuple(ResultInfo::UNKNOWN,  "NO_DIM"), 
   EnumTuple(ResultInfo::SCALAR,  "SCALAR"),
   EnumTuple(ResultInfo::VECTOR,  "VECTOR"),
   EnumTuple(ResultInfo::TENSOR,  "TENSOR"),
   EnumTuple(ResultInfo::STRING,  "STRING")
  };

  Enum<ResultInfo::EntryType> ResultInfo::EntryTypeEnum_ = \
      Enum<ResultInfo::EntryType>("Entry type of Results",
                                  sizeof(entryTypeTuples) / sizeof(EnumTuple),
                                  entryTypeTuples);

}
