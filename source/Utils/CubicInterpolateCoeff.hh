/*
* This file contains only the coefficient matrix for cubic interpolation!
*/

int CIC[4][4] = {
{ 1, 0, 0, 0},
{ 0, 0, 1, 0},
{-3, 3,-2,-1},
{ 2,-2, 1, 1}};
